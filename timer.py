from threading import Thread, Event, Timer
import time
class GbnTimer:
    #delay and period are in seconds
    period = 2

    # for convenience, fucntion have no args
    def __init__(self,function):
        self.function = function


    def start(self):
        # init a new timer , start it
        self.timer = Timer(GbnTimer.period, self.function)
        self.timer.start()

    def stop(self):
        # cancel the timer so it cannot be scheduled by cpu
        self.timer.cancel()

    def restart(self):

        self.stop()
        self.start()
