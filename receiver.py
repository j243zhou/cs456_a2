from packet import Packet
import socket
import argparse

SeqNumModulo = 32

if __name__ == "__main__":


    parser = argparse.ArgumentParser()
    parser.add_argument('emu_addr')
    parser.add_argument('emu_port', type=int)
    parser.add_argument('receive_port', type=int)
    parser.add_argument('file')
    args = parser.parse_args()

    with open("arrival.log", 'a') as log, open(args.file,'a') as output:

        expectedNum = 0

        receive_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        receive_socket.bind(("", args.receive_port))


        while True:
            # receive a packet
            receive_data, addr = receive_socket.recvfrom(512)
            receive_packet = Packet.parseUDPdata(receive_data)
            seq_num = receive_packet.get_seq_num()

            if receive_packet.get_type() != 2:
                log.write("%d\n" % receive_packet.get_seq_num())

            if seq_num == expectedNum:
                # if this is the expected one and an EOT, send back an EOT to end the transimition
                if receive_packet.get_type() == 2:
                    data_to_send = Packet.createEOT(expectedNum).getUDPdata()
                    receive_socket.sendto(data_to_send, (args.emu_addr, args.emu_port))
                    break
                else:
                    # if this is the expected one, write to output, send back acked
                    output.write(receive_packet.get_bytes().decode())

                    data_to_send = Packet.createACK(expectedNum).getUDPdata()
                    receive_socket.sendto(data_to_send, (args.emu_addr, args.emu_port))

                    expectedNum = (expectedNum + 1) % SeqNumModulo

            else:
                # not the expected one , resend the latest acked packet to let sender resend all packets
                # if an ack get lost, the sender's timer will time out and re-send all packets
                last_seq = (expectedNum - 1) % SeqNumModulo

                if last_seq < 0:
                    last_seq = last_seq + SeqNumModulo

                data_to_send = Packet.createACK(last_seq).getUDPdata()
                receive_socket.sendto(data_to_send, (args.emu_addr, args.emu_port))