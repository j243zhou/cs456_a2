from packet import Packet
import socket
import argparse
from timer import GbnTimer

SeqNumModulo = 32
#All packets that have been sent but not yet acked
packets_buffer = []
window_size = 10

base = 0
next_seq = 0

#function determine if the window is full
def is_full(base_para, next_seq_para):
    global window_size

    if base_para + window_size >= SeqNumModulo:
        if base_para <= next_seq_para < SeqNumModulo:
            return False
        elif 0 <= next_seq_para < (base_para + window_size) % SeqNumModulo:
            return False
        else:
            return True
    else:
        return not base_para <= next_seq_para < base_para + window_size


# resend all packets that are in the unack buffer when timer times out
def resend_unacked():
    global send_socket
    global send_log
    global emu_pair

    for p in packets_buffer:
        send_socket.sendto(p.getUDPdata(), emu_pair)
        if p.get_type() != 2:
            seq_log = open("seqnum.log", 'a')
            seq_log.write("%d\n" % p.get_seq_num())
            seq_log.close()


# remove accumatively when acking an packet
def removeFrombuffer(base_para):
    global packets_buffer

    count = 0
    for p in packets_buffer:
        if p.get_seq_num() == base_para:
            break
        else:
            count = count + 1
    packets_buffer = packets_buffer[count:]




if __name__ == "__main__":

    # parse command line argument
    parser = argparse.ArgumentParser()
    parser.add_argument('emu_address')
    parser.add_argument('emu_port', type=int)
    parser.add_argument('ack_port', type=int)
    parser.add_argument('file')

    args = parser.parse_args()


    emu_pair = (args.emu_address, args.emu_port)
    ack_port = args.ack_port
    file = args.file

    send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    send_socket.bind(("", ack_port))


    finished_send = False
    timer = GbnTimer(resend_unacked)

    with  open("ack.log",'a') as ack_log, open(args.file, 'r') as f:

        while True:
            # when window is not full, read a packet and send to emulator
            if not is_full(base, next_seq) and not finished_send:
                data = f.read(500)
                if not data:
                    packet = Packet.createEOT(next_seq)
                    finished_send = 1
                else:
                    packet = Packet.createPacket(next_seq, data)

                packets_buffer.append(packet)
                send_socket.sendto(packet.getUDPdata(), emu_pair)
                if base == next_seq:
                    # start a timer task, resend all sent but unaced packages when time out
                    timer.start()

                if packet.get_type() != 2:
                    seq_log = open("seqnum.log", 'a')
                    seq_log.write("%d\n" % next_seq)
                    seq_log.close()

                next_seq = (next_seq + 1) % SeqNumModulo


            else:
                # when window is full, now receive ack packet
                data, addr = send_socket.recvfrom(512)

                ack_packet = Packet.parseUDPdata(data)
                seq_num = ack_packet.get_seq_num()

                if ack_packet.get_type() != 2:
                    ack_log.write("%d\n" % ack_packet.get_seq_num())

                # slide the window
                base = (seq_num + 1) % SeqNumModulo

                removeFrombuffer(base)

                if base == next_seq:
                    # no more packets to send, either go back to upper section to read more packets
                    # or receive EOT packet to end the transimition
                    timer.stop()

                    if ack_packet.get_type() == 2:
                        break
                else:
                    timer.restart()


