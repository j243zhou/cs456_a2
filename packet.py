

class Packet:

    def __init__(self, type, seqnum, data):
        self.maxDataLength = 500
        self.SeqNumModulo = 32
        if len(data) > self.maxDataLength:
            raise Exception("data too large (max 500 chars)")
        self.type = type
        self.seqnum = seqnum % self.SeqNumModulo
        self.data = data

    @staticmethod
    def createACK(SeqNum):
        return Packet(0, SeqNum, ' ')

    @staticmethod
    def createPacket(SeqNum,data):
        return Packet(1, SeqNum, data)

    @staticmethod
    def createEOT(SeqNum):
        return Packet(2, SeqNum, ' ')

    def get_type(self):
        return self.type

    def get_seq_num(self):
        return self.seqnum

    def get_length(self):
        return len(self.data)

    def get_bytes(self):
        return self.data.encode()

    def getUDPdata(self):
        buf = bytearray(512)

        buf[0:4] = (self.get_type().to_bytes(length=4, byteorder='big'))
        buf[4:8] = (self.get_seq_num().to_bytes(length=4, byteorder='big'))
        buf[8:12] = (self.get_length().to_bytes(length=4, byteorder='big'))
        buf[12:12 + self.get_length()] = (self.get_bytes())

        return buf
    @staticmethod
    def parseUDPdata(UDPdata):
        type = int.from_bytes(UDPdata[0:4], byteorder='big')
        seqnum = int.from_bytes(UDPdata[4:8], byteorder='big')
        length = int.from_bytes(UDPdata[8:12], byteorder='big')
        data = UDPdata[12:12+length].decode()

        return Packet(type, seqnum, data)

#
# if __name__ == "__main__":
#     testPacket = Packet.createPacket(0,"This is a test packet")
#     testAck = Packet.createACK(1)
#     testEOF = Packet.createEOT(2)


