from enum import Enum


class Status(Enum):
     SENT = 1
     ACKED = 2
     OUTSTANDING = 3